Categories:Development
License:Apache2
Web Site:http://mobiperf.com
Source Code:https://github.com/Mobiperf/MobiPerf
Issue Tracker:https://github.com/Mobiperf/MobiPerf/issues

Auto Name:MobiPerf
Summary:Mobile network measurements
Description:
The app uploads 'anonymised' data to the Mobiperf server run by the
developers.
The server is based on free software but the app would need re-coding
to work with a different server.
You can view the data collected that has been collected within the app.

The Android app periodically checks in with the measurement server,
running on Google App Engine,
which sends it a list of measurement tasks to perform. Measurement tasks
include ping, traceroute, HTTP GET, DNS lookup, and TCP Throughput. Each
task has an associated set of measurement parameters (e.g., which host
to ping), and a schedule (periodicity at which to take the measurement).

The device runs the measurements in the background, and uploads the
measurement results on its next checkin cycle. By default, devices
check in with the server every hour. In order to avoid draining the
battery, the app will not take any measurements if the battery is
below a given threshold (80% by default).
.

Repo Type:git
Repo:https://github.com/Mobiperf/MobiPerf.git

Build:v2.1,101
    commit=469323682e
    subdir=android
    target=android-10
    rm=android/tests

Build:v2.4.2,1042
    commit=a0de57708df5051c33
    subdir=android
    target=android-10

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:v2.4.2
Current Version Code:1042

