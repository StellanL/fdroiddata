Categories:Office
License:GPLv3
Web Site:
Source Code:https://github.com/vmihalachi/turbo-editor
Issue Tracker:https://github.com/vmihalachi/turbo-editor/issues
Donate:https://github.com/vmihalachi/turbo-editor
FlattrID:2173527

Auto Name:Turbo Editor
Summary:Simple text editor
Description:
No description available
.

Repo Type:git
Repo:https://github.com/vmihalachi/turbo-editor

Build:1.3,7
    commit=3697dd
    subdir=Turbo Editor
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.3
Current Version Code:7

